# Training Java Fundamental 2022 Batch 2 #

Materi training :

1. Dasar pemrograman Java

    * Compile kode program Java

        ```
        javac NamaFile.java
        ```

        Contoh:

        ```
        javac Halo.java
        ```

    * Compile kode program ke folder tujuan, misalnya ke folder `bin`

        ```
        javac folder1/NamaFile.java folder2/NamaFile.java -d bin
        ```

        * `-d bin` : destinasi compile diarahkan ke folder `bin`

        Contoh :

        ```
        javac src/*.java src/toko/*.java -d bin
        ```
    
    * Menjalankan kode program Java

        ```
        java NamaClass
        ````

        Contoh:

        ```
        java Halo
        ```
    
    * Cara setting classpath

        Windows :

        ```
        set CLASSPATH=C:\Users\endy\belajar-java
        ```

        Mac / Linux :

        ```
        export CLASSPATH=/home/endy/belajar-java
        ```
    
    * Cara membuat file jar

        1. Zip isi folder `bin`
        2. Rename menjadi `namafile.jar`
    
    * Cara menggunakan file jar

        ```
        set CLASSPATH=c:\path\menuju\file.jar
        ```
    
    * Cara melihat isi file jar

        ```
        jar -tvf namafile.jar
        ```

2. Apache Maven

    * Compile Source Code dan Membuat Jar

        ```
        mvn package
        ```
    
    * Menghapus hasil compile

        ```
        mvn clean
        ```

    * Hapus hasil compile, kemudian compile lagi

        ```
        mvn clean package
        ```
    
    * Menjalankan main class dengan Maven

        ```
        mvn clean package exec:java -Dexec.mainClass="Halo"
        ```

3. Struktur Aplikasi Java

    [![Struktur Aplikasi Java](img/struktur-aplikasi-java-class-instance1.jpg)](img/struktur-aplikasi-java-class-instance1.jpg)

4. Object Oriented Programming

    [![Konsep OOP](img/konsep-oop.jpg)](img/konsep-oop.jpg)

5. Class yang sering digunakan
6. Baca tulis data dari file
7. Baca tulis data dari jaringan
8. Multithreading

    [![Thread Unsafe](img/thread-unsafe.jpg)](img/thread-unsafe.jpg)

    [![Liveness Problem](img/thread-liveness.jpg)](img/thread-liveness.jpg)

9. Aplikasi desktop dengan Swing

    [![UI Mockup](img/ui-mockup-data-produk.png)](img/ui-mockup-data-produk.png)

