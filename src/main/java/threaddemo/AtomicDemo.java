package threaddemo;

import java.util.ArrayList;
import java.util.List;

public class AtomicDemo {
    public static void main(String[] args) throws Exception {
        Integer jumlahThread = 50000;
        Integer jumlahUlang = 10;
        AtomicCounter c = new AtomicCounter();

        List<TambahCounter> daftarThreadTambah = new ArrayList<>();
        for(int i = 0; i<jumlahThread; i++){
            daftarThreadTambah.add(new TambahCounter(c, jumlahUlang));
        }

        List<KurangCounter> daftarThreadKurang = new ArrayList<>();
        for(int i = 0; i<jumlahThread; i++){
            daftarThreadKurang.add(new KurangCounter(c, jumlahUlang));
        }

        // start semua thread
        for(int i = 0; i<jumlahThread; i++){
            daftarThreadTambah.get(i).start();
            daftarThreadKurang.get(i).start();
        }

        // join ke semua thread
        for(int i = 0; i<jumlahThread; i++){
            daftarThreadTambah.get(i).join();
            daftarThreadKurang.get(i).join();
        }

        System.out.println("============= Posisi Akhir =============");
       c.show();
    }

    static class TambahCounter extends Thread {
        private Integer ulang;
        private AtomicCounter counter;

        public TambahCounter(AtomicCounter c, Integer u){
            this.counter = c;
            this.ulang = u;
        }

        public void run(){
            for(int i=0; i<ulang; i++){
                counter.tambah();
            }
        }
    }

    static class KurangCounter extends Thread {
        private Integer ulang;
        private AtomicCounter counter;

        public KurangCounter(AtomicCounter c, Integer u){
            this.counter = c;
            this.ulang = u;
        }

        public void run(){
            for(int i=0; i<ulang; i++){
                counter.kurang();
            }
        }
    }
}
