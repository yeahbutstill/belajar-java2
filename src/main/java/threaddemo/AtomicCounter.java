package threaddemo;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicCounter {
    private AtomicInteger angka = new AtomicInteger(0);
    private Integer ulang = 10;

    public void hitung() throws Exception {
        for(int i=0; i<ulang; i++) {
            angka.incrementAndGet();
            System.out.println(Thread.currentThread().getName() + " : "+angka.get());
            Thread.sleep(1 * 1000);
        }
    }

    public void tambah(){
        angka.incrementAndGet();
        show();
    }

    public void kurang(){
        angka.decrementAndGet();
        show();
    }

    public void show(){
        System.out.println("Angka : "+angka.get());
    }
}
