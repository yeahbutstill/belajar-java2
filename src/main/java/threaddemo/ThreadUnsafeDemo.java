package threaddemo;

import java.util.ArrayList;
import java.util.List;

public class ThreadUnsafeDemo {

    public static void main(String[] args) throws Exception {
        Integer jumlahThread = 50000;
        Integer jumlahUlang = 10;
        Counter c = new Counter();

        List<TambahCounter> daftarThreadTambah = new ArrayList<>();
        for(int i = 0; i<jumlahThread; i++){
            daftarThreadTambah.add(new TambahCounter(c, jumlahUlang));
        }

        List<KurangCounter> daftarThreadKurang = new ArrayList<>();
        for(int i = 0; i<jumlahThread; i++){
            daftarThreadKurang.add(new KurangCounter(c, jumlahUlang));
        }

        // start semua thread
        for(int i = 0; i<jumlahThread; i++){
            daftarThreadTambah.get(i).start();
            daftarThreadKurang.get(i).start();
        }

        // join ke semua thread
        for(int i = 0; i<jumlahThread; i++){
            daftarThreadTambah.get(i).join();
            daftarThreadKurang.get(i).join();
        }

        System.out.println("============= Posisi Akhir =============");
       c.show();
    }

    static class TambahCounter extends Thread {
        private Integer ulang;
        private Counter counter;

        public TambahCounter(Counter c, Integer u){
            this.counter = c;
            this.ulang = u;
        }

        public void run(){
            for(int i=0; i<ulang; i++){
                counter.tambah();
            }
        }
    }

    static class KurangCounter extends Thread {
        private Integer ulang;
        private Counter counter;

        public KurangCounter(Counter c, Integer u){
            this.counter = c;
            this.ulang = u;
        }

        public void run(){
            for(int i=0; i<ulang; i++){
                counter.kurang();
            }
        }
    }
}
