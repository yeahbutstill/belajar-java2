package chat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class ChatServer {
    private Integer port;

    public ChatServer(Integer port) {
        this.port = port;
    }

    public void start(){
        System.out.println("Menunggu koneksi di port "+port);

        // Input Stream dan Output Stream untuk bertukar data
        try (
            ServerSocket serverSocket = new ServerSocket(port);
            Socket socket = serverSocket.accept();
            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter writer = new PrintWriter(socket.getOutputStream(), true)) {

            System.out.println("Ada client connect dari IP "+socket.getInetAddress());

            // handle message dari client
            String data;
            while((data = reader.readLine()) != null) {
                String reply = data.toUpperCase();
                System.out.println("C>"+data);
                writer.println(reply);
                if("quit".equalsIgnoreCase(data)) {
                    break;
                }
            }
        } catch (IOException err) {
            err.printStackTrace();
        }
    }
}
