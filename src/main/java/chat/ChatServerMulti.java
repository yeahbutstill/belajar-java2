package chat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class ChatServerMulti {
    private Integer port;
    private Boolean running = true;

    public ChatServerMulti(Integer port) {
        this.port = port;
    }

    public void start(){
        System.out.println("Menunggu koneksi di port "+port);

        // Input Stream dan Output Stream untuk bertukar data
        try (ServerSocket serverSocket = new ServerSocket(port)){
            List<Thread> daftarKoneksi = new ArrayList<>();
            while(running) {
                Socket socket = serverSocket.accept();
                System.out.println("Running : "+running);
                //new ClientHandler(socket).start(); // ini kalau extends Thread
                Thread t = new Thread(new ClientHandler(socket));
                daftarKoneksi.add(t);
                t.start(); // ini kalau implements Runnable
            }
            System.out.println("Selesai menerima koneksi");

            // menunggu semua thread selesai
            for(Thread tx : daftarKoneksi){
                tx.join();
            }

            // setelah semua thread selesai, baru jalankan perintah berikut ini
            System.out.println("Semua koneksi sudah ditutup");
            
        } catch (IOException | InterruptedException err) {
            err.printStackTrace();
        }
    }

    //class ClientHandler extends Thread {
    class ClientHandler implements Runnable {
        private Socket socket;

        public ClientHandler(Socket s){
            this.socket = s;
            //System.out.println("Menerima client di thread "+getName()); // ini kalau extends Thread
            System.out.println("Menerima client di thread "+Thread.currentThread().getName());
        }

        public void run(){
            // Input Stream dan Output Stream untuk bertukar data
            try (
                BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                PrintWriter writer = new PrintWriter(socket.getOutputStream(), true)) {

                System.out.println("Ada client connect dari IP "+socket.getInetAddress());

                // handle message dari client
                String data;
                while((data = reader.readLine()) != null) {
                    String reply = data.toUpperCase();
                    System.out.println("C>"+data);
                    writer.println(reply);

                    if("stop".equalsIgnoreCase(data)){
                        ChatServerMulti.this.running = false;
                    }

                    if("quit".equalsIgnoreCase(data)) {
                        break;
                    }
                }
            } catch (IOException err) {
                err.printStackTrace();
            }
        }
    }
}
