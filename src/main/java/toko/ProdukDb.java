package toko;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

public class ProdukDb {
    private static final String NAMA_FILE = "produk.txt";
    private List<Produk> dataProduk = new ArrayList<>();

    public ProdukDb() throws URISyntaxException, IOException {
        Charset charset = Charset.forName("UTF-8");
        URL urlFile = ClassLoader.getSystemResource(NAMA_FILE);
        if(urlFile == null) {
            System.out.println("File tidak ditemukan");
            return;
        }
        Path pathDbProduk = Path.of(urlFile.toURI());

        System.out.println("Ukuran file database : "+Files.size(pathDbProduk));
        
        BasicFileAttributes attr = Files.readAttributes(pathDbProduk, BasicFileAttributes.class);
        System.out.println("Terakhir dibuka : "+attr.lastAccessTime());
        System.out.println("Terakhir diedit : "+attr.lastModifiedTime());
        
        BufferedReader reader = Files.newBufferedReader(pathDbProduk, charset);
        String line = reader.readLine();
        while ((line = reader.readLine()) != null) {
            String[] data = line.split(",");
            
            Produk p = new Produk();
            p.setKode(data[0]);
            p.setNama(data[1]);
            p.setHarga(new BigDecimal(data[2]));
            dataProduk.add(p);
        }
    }

    public List<Produk> semuaProduk(){
        return dataProduk;
    }

    public Produk cariByKode(String kode){
        for (Produk produk : dataProduk) {
            if(kode.equalsIgnoreCase(produk.getKode())) {
                return produk;
            }
        }
        return null;
    }

    public void save() throws URISyntaxException {
        Charset charset = Charset.forName("UTF-8");
        Path pathDbProduk = Path.of(ClassLoader.getSystemResource(NAMA_FILE).toURI());
        try (BufferedWriter writer = Files.newBufferedWriter(pathDbProduk, charset)) {
            String s = "kode,nama,harga";
            writer.write(s, 0, s.length());
            for (Produk produk : dataProduk) {
                s = "\r\n";
                s += produk.getKode();
                s += ",";
                s += produk.getNama();
                s += ",";
                s += produk.getHarga().setScale(0, RoundingMode.HALF_EVEN);
                writer.write(s, 0, s.length());
            }
        } catch (IOException x) {
            System.err.format("IOException: %s%n", x);
        }
    }

    public void hapus(int index){
        try {
            dataProduk.remove(index);
            save();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }
}
