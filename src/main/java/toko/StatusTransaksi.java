package toko;

public enum StatusTransaksi {
    BELUM_DIBAYAR,
    LUNAS,
    DIPROSES,
    DALAM_PENGIRIMAN,
    DITERIMA,
    SELESAI,
    KOMPLAIN
}
