package toko;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import lombok.Getter;
import lombok.Setter;

@Setter @Getter
public class Pembelian {
    private StatusTransaksi status = StatusTransaksi.BELUM_DIBAYAR;
    private LocalDateTime waktuTransaksi = LocalDateTime.now();
    private Customer customer;
    private List<PembelianDetail> daftarPembelianDetail = new LinkedList<>();
    private List<Diskon> daftarDiskon = new ArrayList<>();
    

    public BigDecimal total(){
        BigDecimal hasil = BigDecimal.ZERO;

        for (PembelianDetail pembelianDetail : daftarPembelianDetail) {
            hasil = hasil.add(pembelianDetail.subtotal());
        }
        
        return hasil;
    }

    public BigDecimal totalDiskon(){
        BigDecimal hasil = BigDecimal.ZERO;
        for(Diskon d : daftarDiskon) {
            hasil = hasil.add(d.hitung(this));
        }
        return hasil;
    }

    public BigDecimal totalBayar(){
        return total().subtract(totalDiskon());
    }

    public void tampilkanRincianDiskon() {
        NumberFormat formatter = DecimalFormat
        .getCurrencyInstance(
            new Locale("in", "id"));

        for(Diskon d : daftarDiskon){
            System.out.println(d.getClass().getSimpleName() 
            + " : "
            + formatter.format(
                d.hitung(this).setScale(0, RoundingMode.HALF_EVEN)));
        }
    }
}
