package toko.ui;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Locale;

public class FormatAngkaHelper {
    public static String formatAngkaUang(BigDecimal angka){
        return DecimalFormat
        .getCurrencyInstance(new Locale("in", "id"))
        .format(angka.setScale(0, RoundingMode.HALF_EVEN));
    }
}
