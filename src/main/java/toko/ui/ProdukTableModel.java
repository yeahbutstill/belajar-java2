package toko.ui;

import java.io.IOException;
import java.math.RoundingMode;
import java.net.URISyntaxException;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Locale;

import javax.swing.table.AbstractTableModel;

import toko.Produk;

public class ProdukTableModel extends AbstractTableModel {

    
    private List<Produk> dataProduk;

    public ProdukTableModel(List<Produk> dataProduk){
        this.dataProduk = dataProduk;
    }

    @Override
    public int getRowCount() {
        return dataProduk.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0: return "Kode";
            case 1: return "Nama";
            case 2: return "Harga";
            default: return "";
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Produk p = dataProduk.get(rowIndex);
        switch (columnIndex) {
            case 0: return p.getKode();
            case 1: return p.getNama();
            case 2: return FormatAngkaHelper.formatAngkaUang(p.getHarga());
            default: return "";
        }
    }
    
}
