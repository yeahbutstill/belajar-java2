import java.time.LocalDateTime;

import toko.Customer;

public class Halo {
    public static void main(String[] args) {
        System.out.println("Selamat datang di Training Java Fundamental");
        System.out.println("Waktu saat ini : "+LocalDateTime.now());

        Customer c = new Customer();
        c.setEmail("endy.muhardin@gmail.com");
        c.setNama("Endy Muhardin");

        System.out.println("Nama Customer : "+c.getNama());
        System.out.println("Email Customer : "+c.getEmail());
    }
}

// statement harus di dalam method
// System.out.println("Halo");

// method harus di dalam class
//public static void halo(String[] x) {
//    System.out.println("Halo");
//}